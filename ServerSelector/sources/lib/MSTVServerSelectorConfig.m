//
//  MSTVServerSelectorConfig.m
//  ingdirect
//
//  Created by Remy Bardou on 14/04/2015.
//  Copyright (c) 2015 MyStudioFactory. All rights reserved.
//

#import "MSTVServerSelectorConfig.h"
#import "MSTVServerSelectorEnvironment.h"

// user-defaults
NSString * const kServerSelectorUserDefaultsLastEnvKey          = @"server-selector-last-env";
NSString * const kServerSelectorUserDefaultsAutoEnvKey          = @"server-selector-auto-env";
NSString * const kServerSelectorUserDefaultsNoCancelKey			= @"server-selector-no-cancel";

// Plist
NSString * const kServerSelectorPlistConfigFileName             = @"server-selector-config";

// top-level nodes
NSString * const kServerSelectorConfigurationKey                = @"config";
NSString * const kServerSelectorEnvironmentsKey                 = @"environments";

// configuration keys
NSString * const kServerSelectorConfigurationLoadingDelayKey    = @"autoloading_delay";

NSString * const kServerSelectorConfigurationAltColorKey        = @"color_alternate";
NSString * const kServerSelectorConfigurationCellColorKey       = @"color_cell";
NSString * const kServerSelectorConfigurationCellProduction     = @"color_cell_production";
NSString * const kServerSelectorConfigurationCellSepKey         = @"color_cell_separator";
NSString * const kServerSelectorConfigurationHeaderColorKey     = @"color_header_bg";
NSString * const kServerSelectorConfigurationHeaderSepKey       = @"color_header_separator";
NSString * const kServerSelectorConfigurationMainColorKey       = @"color_main";

NSString * const kServerSelectorConfigurationHostTimeoutKey     = @"host_timeout";
NSString * const kServerSelectorConfigurationHostHttpHeaderKey  = @"host_http_header";
NSString * const kServerSelectorConfigurationHostStatusCodeKey  = @"host_status_code_expected";

// default values
CGFloat const kServerSelectorProgressBarTimerRate = 1/60.0f;
CGFloat const kServerSelectorConfigurationLoadingDelayDefaultValue = 3.0f;

static NSString* stringInDic(NSDictionary* dic, NSString* key, NSString* defaultValue) {
    if (dic[key]) {
        return dic[key];
    }
    return defaultValue;
}

static double doubleInDic(NSDictionary *dic, NSString *key, double defaultValue) {
    if (dic[key]) {
        return [dic[key] doubleValue];
    }
    return defaultValue;
}

static long longInDic(NSDictionary *dic, NSString *key, long defaultValue) {
    if (dic[key]) {
        return [dic[key] longValue];
    }
    return defaultValue;
}

static UIColor* colorWithHexString(NSString* hexStrValue) {
    
    if (! hexStrValue) return nil;
    if (![hexStrValue isKindOfClass:[NSString class]]) return nil;
    if (([hexStrValue length] != 6) && ([hexStrValue length] != 8)) return nil;
    
    unsigned int r = 0, g = 0, b = 0;
    const char *hexCStr = [hexStrValue UTF8String];
    
    if ([hexStrValue length] == 6) {
        sscanf(hexCStr, "%02X%02X%02X", &r, &g, &b);
    } else {
        if ([hexStrValue hasPrefix:@"0x"]) sscanf(hexCStr, "0x%02X%02X%02X", &r, &g, &b);
        else if ([hexStrValue hasPrefix:@"0X"]) sscanf(hexCStr, "0X%02X%02X%02X", &r, &g, &b);
        else return nil;
    }
    
    return [UIColor colorWithRed:(((CGFloat)r)/255.0f) green:(((CGFloat)g)/255.0f) blue:(((CGFloat)b)/255.0f) alpha:1.0f];
}

@interface MSTVServerSelectorConfig ()

@property (nonatomic, strong) UIColor *altColor;
@property (nonatomic, strong) UIColor *cellColor;
@property (nonatomic, strong) UIColor *cellProductionColor;
@property (nonatomic, strong) UIColor *cellSeparatorColor;
@property (nonatomic, strong) UIColor *headerColor;
@property (nonatomic, strong) UIColor *headerSeparatorColor;
@property (nonatomic, strong) UIColor *mainColor;

@property (nonatomic, assign) CGFloat hostTimeout;
@property (nonatomic, strong) NSString *hostHttpHeader;
@property (nonatomic, assign) NSInteger hostStatusCodeExpected; // 0 for ANY

@property (nonatomic, assign) double autoloadingDelay;

@property (nonatomic, strong) NSArray *environments;

@property (nonatomic, assign) BOOL shouldAutoloadEnvironment;
@property (nonatomic, strong) NSString *environmentKeyToAutoload;

@end

@implementation MSTVServerSelectorConfig

- (id) init {
    self = [super init];
    if (!self) return nil;
    
    [self loadConfig];
    
    return self;
}

- (void) loadConfig {
    NSDictionary *dic = [[NSDictionary alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:kServerSelectorPlistConfigFileName
                                                                            ofType:@"plist"]];
    
    /** LOAD CONFIG **/
    NSDictionary *config = dic[kServerSelectorConfigurationKey];
    
    // load color informations
    self.altColor = colorWithHexString(stringInDic(config, kServerSelectorConfigurationAltColorKey, @"0xFFFFFF"));
    self.cellColor = colorWithHexString(stringInDic(config, kServerSelectorConfigurationCellColorKey, @"0x000000"));
    self.cellProductionColor = colorWithHexString(stringInDic(config, kServerSelectorConfigurationCellProduction, @"0x000000"));
    self.cellSeparatorColor = colorWithHexString(stringInDic(config, kServerSelectorConfigurationCellSepKey, @"0x000000"));
    self.headerColor = colorWithHexString(stringInDic(config, kServerSelectorConfigurationHeaderColorKey, @"0xFFFFFF"));
    self.headerSeparatorColor = colorWithHexString(stringInDic(config, kServerSelectorConfigurationHeaderSepKey, @"0x000000"));
    self.mainColor = colorWithHexString(stringInDic(config, kServerSelectorConfigurationMainColorKey, @"0xFFFFFF"));
    
    // load ux behavior
    self.autoloadingDelay = doubleInDic(config, kServerSelectorConfigurationLoadingDelayKey, kServerSelectorConfigurationLoadingDelayDefaultValue);
    
    // load ping informations
    self.hostTimeout = doubleInDic(config, kServerSelectorConfigurationHostTimeoutKey, 2.0f);
    self.hostHttpHeader = stringInDic(config, kServerSelectorConfigurationHostHttpHeaderKey, @"HEAD");
    self.hostStatusCodeExpected = longInDic(config, kServerSelectorConfigurationHostStatusCodeKey, 0);
    
    
    /** LOAD ENVIRONMENTS **/
    NSArray *dicEnvs = dic[kServerSelectorEnvironmentsKey];
    
    NSMutableArray *envs = [[NSMutableArray alloc] init];
    [dicEnvs enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
        MSTVServerSelectorEnvironment *environment = [[MSTVServerSelectorEnvironment alloc] initWithDictionary:obj config:self];
        [envs addObject:environment];
    }];
    [envs sortUsingComparator:^NSComparisonResult(MSTVServerSelectorEnvironment *env1, MSTVServerSelectorEnvironment *env2) {
        NSInteger order1 = env1.displayOrder;
        NSInteger order2 = env2.displayOrder;
        
        if (order1 != 0 && order2 != 0) {
            if (order1 > order2) return NSOrderedDescending;
            else if (order2 > order1) return NSOrderedAscending;
        } else if (order1 != 0 && order2 == 0) {
            return NSOrderedAscending;
        } else if (order1 == 0 && order2 != 0) {
            return NSOrderedDescending;
        }
        
        // else, order by name
        NSString *name1 = env1.displayName;
        NSString *name2 = env2.displayName;
        
        return [name1 compare:name2];
    }];
    
    self.environments = [envs copy];
}

#pragma mark - Environments

- (NSString *) environmentKeyToAutoload {
    if (!_environmentKeyToAutoload) {
        _environmentKeyToAutoload = [[NSUserDefaults standardUserDefaults] stringForKey:kServerSelectorUserDefaultsAutoEnvKey];
    }
    return _environmentKeyToAutoload;
}

- (MSTVServerSelectorEnvironment *) environmentToAutoload {
    NSString *env = self.environmentKeyToAutoload;
    
    __block MSTVServerSelectorEnvironment *configuration = nil;
    [self.environments enumerateObjectsUsingBlock:^(MSTVServerSelectorEnvironment *obj, NSUInteger idx, BOOL *stop) {
        if ([obj.displayName isEqualToString:env]) {
            *stop = YES;
            configuration = obj;
        }
    }];
    
    return configuration;
}

- (BOOL) shouldAutoloadEnvironment {
    if (self.environmentKeyToAutoload) {
        NSString *env = self.environmentKeyToAutoload;
        __block BOOL envExists = NO;
        
        [self.environments enumerateObjectsUsingBlock:^(MSTVServerSelectorEnvironment *obj, NSUInteger idx, BOOL *stop) {
            if ([obj.displayName isEqualToString:env]) {
                *stop = YES;
                envExists = YES;
            }
        }];
        
        if (!envExists)
            [self forgetAutoloadingEnvironment];
        
        return envExists;
    }
    return NO;
}

- (void) saveAutoloadingEnvironment:(MSTVServerSelectorEnvironment *)environment {
    [[NSUserDefaults standardUserDefaults] setObject:environment.uniqueName forKey:kServerSelectorUserDefaultsAutoEnvKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    _environmentKeyToAutoload = nil;
}

- (void) forgetAutoloadingEnvironment {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kServerSelectorUserDefaultsAutoEnvKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    _environmentKeyToAutoload = nil;
}

- (void) saveLastLoadedEnvironment:(MSTVServerSelectorEnvironment *)env {
    [[NSUserDefaults standardUserDefaults] setObject:env.uniqueName forKey:kServerSelectorUserDefaultsLastEnvKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL) environmentIsLastLoadedEnvironment:(MSTVServerSelectorEnvironment *)env {
    NSString *key = [[NSUserDefaults standardUserDefaults] stringForKey:kServerSelectorUserDefaultsLastEnvKey];
    return ([key isEqualToString:env.uniqueName]);
}

- (MSTVServerSelectorEnvironment *) environmentWithName:(NSString *)envName {
    __block MSTVServerSelectorEnvironment *env = nil;
    [self.environments enumerateObjectsUsingBlock:^(MSTVServerSelectorEnvironment *obj, NSUInteger idx, BOOL *stop) {
        if ([[obj.uniqueName uppercaseString] isEqualToString:[envName uppercaseString]]) {
            *stop = YES;
            env = obj;
        }
    }];
    return env;
}

- (void) enableNoCancel {
	[[NSUserDefaults standardUserDefaults] setObject:@YES forKey:kServerSelectorUserDefaultsNoCancelKey];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL) noCancelEnabled {
	return [[[NSUserDefaults standardUserDefaults] objectForKey:kServerSelectorUserDefaultsNoCancelKey] boolValue];
}

@end
