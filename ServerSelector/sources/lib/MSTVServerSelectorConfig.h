//
//  MSTVServerSelectorConfig.h
//  ingdirect
//
//  Created by Remy Bardou on 14/04/2015.
//  Copyright (c) 2015 MyStudioFactory. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class MSTVServerSelectorEnvironment;

extern CGFloat const kServerSelectorProgressBarTimerRate;

@interface MSTVServerSelectorConfig : NSObject

// ui-config
@property (nonatomic, readonly) UIColor *altColor;
@property (nonatomic, readonly) UIColor *cellColor;
@property (nonatomic, readonly) UIColor *cellProductionColor;
@property (nonatomic, readonly) UIColor *cellSeparatorColor;
@property (nonatomic, readonly) UIColor *headerColor;
@property (nonatomic, readonly) UIColor *headerSeparatorColor;
@property (nonatomic, readonly) UIColor *mainColor;

@property (nonatomic, readonly) CGFloat hostTimeout;
@property (nonatomic, readonly) NSString *hostHttpHeader;
@property (nonatomic, readonly) NSInteger hostStatusCodeExpected; // 0 for ANY

@property (nonatomic, readonly) double autoloadingDelay;

// env-config
@property (nonatomic, readonly) NSArray *environments;

// convenience access for auto loading
@property (nonatomic, readonly) BOOL noCancelEnabled;
@property (nonatomic, readonly) BOOL shouldAutoloadEnvironment;
@property (nonatomic, readonly) NSString *environmentKeyToAutoload;
@property (nonatomic, readonly) MSTVServerSelectorEnvironment *environmentToAutoload;
- (void) saveAutoloadingEnvironment:(MSTVServerSelectorEnvironment *)environment;
- (void) forgetAutoloadingEnvironment;

- (void) saveLastLoadedEnvironment:(MSTVServerSelectorEnvironment *)env;
- (BOOL) environmentIsLastLoadedEnvironment:(MSTVServerSelectorEnvironment *)env;

- (MSTVServerSelectorEnvironment *) environmentWithName:(NSString *)envName;

- (void) enableNoCancel;

@end
