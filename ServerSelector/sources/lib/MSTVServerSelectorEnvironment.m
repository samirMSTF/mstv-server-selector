//
//  MSTVServerSelectorEnvironment.m
//  ingdirect
//
//  Created by Remy Bardou on 17/04/2015.
//  Copyright (c) 2015 MyStudioFactory. All rights reserved.
//

#import "MSTVServerSelectorEnvironment.h"

// environment keys
NSString * const kServerSelectorEnvironmentDisplayNameKey       = @"display_name";
NSString * const kServerSelectorEnvironmentDisplayOrderKey      = @"display_order";
NSString * const kServerSelectorEnvironmentHostUrlKey           = @"host_url";
NSString * const kServerSelectorEnvironmentIsProdKey            = @"is_prod";
NSString * const kServerSelectorEnvironmentPayloadKey           = @"payload";

/** this is just quick n dirty implementation, there are better ways to do this */
static NSString * camelCaseFromString(NSString *input) {
    NSString *capitalized = [[input capitalizedString] stringByReplacingOccurrencesOfString:@"_" withString:@""];
    
    // make first letter lowercase
    NSString *returnValue = [NSString stringWithFormat:@"%@%@",
                             [[capitalized substringToIndex:1] lowercaseString],
                             [capitalized substringFromIndex:1]
                             ];
    return returnValue;
}

@interface MSTVServerSelectorEnvironment ()

@property (nonatomic, strong) NSString *displayName;
@property (nonatomic, assign) NSInteger displayOrder;
@property (nonatomic, strong) NSString *hostUrl;
@property (nonatomic, assign) BOOL isProd;
@property (nonatomic, strong) NSDictionary *payload;

@property (nonatomic, strong) MSTVServerSelectorConfig *config;

@end

@implementation MSTVServerSelectorEnvironment

- (id) initWithDictionary:(NSDictionary *)dictionary config:(MSTVServerSelectorConfig *)config {
    self = [super init];
    if (!self) return nil;
    
    self.config = config;
    
    [dictionary enumerateKeysAndObjectsUsingBlock:^(NSString *key, id obj, BOOL *stop) {
        [self setValue:obj forKey:camelCaseFromString(key)];
    }];
    
    return self;
}

- (NSString *) uniqueName {
    return self.displayName;
}

- (void) setValue:(id)value forUndefinedKey:(NSString *)key {
    NSLog(@"%@ property does not exist on %@", key, self.class);
}

@end
